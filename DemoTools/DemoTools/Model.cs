﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTools
{
    public class Model : INotifyPropertyChanged
    {
        public string ButtonSign { get; set; }

        private string _text;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Text"));

                    System.Diagnostics.Debug.WriteLine("Text has chaned to:"+ _text);
                }
            }
        }

        public string Title { get; set; }
    }
}
