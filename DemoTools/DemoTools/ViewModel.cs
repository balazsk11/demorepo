﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTools
{
    class ViewModel
    {
        Model model = new Model();

        public Model Model
        {
            get { return model; }
            set { model = value; }
        }

        public ViewModel()
        {
            Model.ButtonSign = "ButtonSign";
            Model.Text = "Text";
            Model.Title = "Title";

        }
    }
}
