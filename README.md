This repo shows basic software engineering skills in c#

Contains the following:
- store data in hard drive
- navigates
- dependency injection
- CRUD: (Create, Read, Update, Delete data)
- input validation
- encryption/decryption
- MVVM architecture